| Language | Framework | Platform | Author |
| -------- | -------- |--------|--------|
| Nodejs | Express | Azure Web App, Virtual Machine| |


# Nodejs Express web application

Sample Nodejs Express web application built using Visual Studio 2017.

## License:

See [LICENSE](LICENSE).

## Contributing

This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/). For more information see the [Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or contact [opencode@microsoft.com](mailto:opencode@microsoft.com) with any additional questions or comments.

## Solution Approach
 ### At First, created a GitLab Project to push NodeJs Web App source code

 ### Created a Dockerfile to keep all the commands to package the node web app

 ### Created K8s Deployment and Service yaml files

 ### Created a “.gitlab-ci.yml” file to run CI/CD Pipeline

 ### Provisioned Kubernetes Cluster Azure AKS with Free Microsoft Azure Account
